# SoSe24_SE_AFMI_Git

Materials for summer semester 2024 lecture "Introduction to Git" as part of [Arbeits- und Forschungsmethoden der Informatik](https://www.informatik.hu-berlin.de/de/forschung/gebiete/se/teaching/ss2024/fmi/fmi).
